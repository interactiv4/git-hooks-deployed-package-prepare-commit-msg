# Interactiv4 - Git Hooks Deployed Package - prepare-commit-msg - (Abadoned)

Description
-----------
**THIS MODULE HAS BEEN ABANDONED AND GROUPED IN THE FOLLOWING REPOSITORY:**

- (https://bitbucket.org/interactiv4/git-hooks-deployed-package)

Versioning
----------
This package follows semver versioning.


Compatibility
-------------
- PHP ^8.1


Installation Instructions
-------------------------
It can be installed manually using composer, by typing the following command:

`composer require --dev interactiv4/git-hooks-deployed-package-prepare-commit-msg --update-with-all-dependencies`


Support
-------
Refer to [issue tracker](https://bitbucket.org/interactiv4/git-hooks-deployed-package-prepare-commit-msg/issues) to open an issue if needed.


Credits
---------
Supported and maintained by Interactiv4 Team.


Contribution
------------
Any contribution is highly appreciated.
The best way to contribute code is to open a [pull request on Bitbucket](https://bitbucket.org/interactiv4/git-hooks-deployed-package-prepare-commit-msg/pull-requests/new).


License
-------
[MIT](https://es.wikipedia.org/wiki/Licencia_MIT)


Copyright
---------
Copyright (c) 2020 Interactiv4 S.L.